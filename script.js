/*
Экранирование это действие, направленное на указание браузера распознать специальный символ, следующий за экраном как обычный симовол, а не часть управляющего кода. Для экранирования символа применяется обратная косая черта \ . Если мы хотим отобразить сам символ косой черты \ то его нужно экранировать таким же образом, то есть его нужно удвоить - \\  .
*/
const newUser = сreateNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

function сreateNewUser() {
  const user = {
    firstName: (firstName = prompt("Enter your name, please")),
    lastName: (lastName = prompt("Enter your last name, please")),
    birthday: (birthday = prompt("Enter your birthday, please", "dd.mm.yyyy")),
    getLogin: function () {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getAge: function () {
      const birthdayMs = Date.parse(birthday.split(".").reverse());
      const userAge = Math.floor(
        (new Date().getTime() - birthdayMs) / (1000 * 3600 * 24 * 365.2425)
      );
      return userAge;
    },
    getPassword: function () {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6, 10)
      );
    },
  };
  return user;
}
